/* eslint-disable no-shadow */
const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('product routes DELETE', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(5);

  t.test('requests the DELETE "/api/v1/product/3fa85f64-5717-4562-b3fc-2c963f66afa6" route success delete item', async tI => {
    const response = await fastify.inject({
      method: 'DELETE',
      url: '/api/v1/product/3fa85f64-5717-4562-b3fc-2c963f66afa6',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(Object.keys(responseBody).length, 0);
  });

  t.test('requests the DELETE "/api/v1/product/111" route failed delete item (invalid id format)', async tI => {
    const response = await fastify.inject({
      method: 'DELETE',
      url: '/api/v1/product/111',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'params.uuid should match format "uuid"');
  });

  t.test('requests the DELETE "/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf" route failed delete item no product', async tI => {
    const response = await fastify.inject({
      method: 'DELETE',
      url: '/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'product with uuid \'127698ee-2383-4670-930f-7620ea1cb5cf\' doesnt exists');
    tI.equal(responseBody.error, 'DELETE_PRODUCT');
  });

  t.test('requests the DELETE "/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf" route failed delete item empty database', async tI => {
    await Products.destroy({
      where: {},
      truncate: true,
    });
    const response = await fastify.inject({
      method: 'DELETE',
      url: '/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'product with uuid \'127698ee-2383-4670-930f-7620ea1cb5cf\' doesnt exists');
    tI.equal(responseBody.error, 'DELETE_PRODUCT');
  });

  t.test('requests the DELETE "/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf" route failed delete item miss database', async tI => {
    await Products.drop();
    const response = await fastify.inject({
      method: 'DELETE',
      url: '/api/v1/product/127698ee-2383-4670-930f-7620ea1cb5cf',
    });

    tI.equal(response.statusCode, 500, 'returns a status code of 500');
  });
});
