/* eslint-disable no-shadow */
const crypto = require('crypto');
const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('cart routes GET list', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(5);

  t.test('requests the "/api/v1/cart" route success', async tI => {
    const items = productMocks.filter(item => item.price <= 4).map(item => item.id);
    const total = productMocks.filter(item => item.price <= 4).reduce((acc, curr) => { acc += curr.price; return acc; }, 0);
    try {
      const response = await fastify.inject({
        method: 'PUT',
        url: '/api/v1/cart',
        body: items,
      });

      const responseBody = JSON.parse(response.body);

      tI.equal(response.statusCode, 200, 'returns a status code of 200');
      tI.equal(responseBody.items.length, items.length);
      tI.equal(responseBody.total, total);
    } catch (err) {
      console.error(err);
    }
    tI.end();
  });

  t.test('requests the "/api/v1/cart" route success discount for every 5', async tI => {
    const { id, price } = productMocks[0];
    const items = [...Array(5).keys()].map(() => id);
    const total = (price * 4 >= 20) ? price * 4 - 1 : price * 4;
    const response = await fastify.inject({
      method: 'PUT',
      url: '/api/v1/cart',
      body: items,
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(responseBody.items.length, 1);
    tI.equal(responseBody.total, total);
    tI.end();
  });

  t.test('requests the "/api/v1/cart" route success discount -1 if total more 20', async tI => {
    const { id, price } = productMocks[4];
    const items = [...Array(3).keys()].map(() => id);
    const total = (price * 3 >= 20) ? price * 3 - 1 : price * 3;
    const response = await fastify.inject({
      method: 'PUT',
      url: '/api/v1/cart',
      body: items,
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(responseBody.items.length, 1);
    tI.equal(responseBody.items[0].qty, 3);
    tI.equal(responseBody.total, total);
    tI.end();
  });

  t.test('requests the "/api/v1/cart" route failed if total more 100', async tI => {
    const { id } = productMocks[4];
    const items = [...Array(20).keys()].map(() => id);
    const response = await fastify.inject({
      method: 'PUT',
      url: '/api/v1/cart',
      body: items,
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'Total amount cannot exceed 100$');
    tI.end();
  });

  t.test('requests the "/api/v1/cart" route failed no product', async tI => {
    const id = crypto.randomUUID();
    const response = await fastify.inject({
      method: 'PUT',
      url: '/api/v1/cart',
      body: [id],
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, `no product with uuid ${id}`);
    tI.end();
  });
});
