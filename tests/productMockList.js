const crypto = require('crypto');

module.exports = [
  {
    id: crypto.randomUUID(), title: 'AAA', price: 1.5, qty: 300,
  },
  {
    id: crypto.randomUUID(), title: 'BBB', price: 3.5, qty: 400,
  },
  {
    id: crypto.randomUUID(), title: 'CCC', price: 2.5, qty: 500,
  },
  {
    id: crypto.randomUUID(), title: 'DDD', price: 3.6, qty: 400,
  },
  {
    id: crypto.randomUUID(), title: 'EEE', price: 7.2, qty: 200,
  },
  {
    id: '3fa85f64-5717-4562-b3fc-2c963f66afa6', title: 'real', price: 13.44, qty: 800,
  },
];
