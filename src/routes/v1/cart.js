const {
  getCartOpts,
} = require('../../schemas/routesSchemas');

const models = require('../../../models');

const Products = models.Products;

module.exports = async function (fastify, opts, done) {
  await Products.sync();
  fastify.put('/cart', getCartOpts);
  done();
};
