const crypto = require('crypto');

const models = require('../../../models');

const Products = models.Products;

const getProductList = async (request, reply) => {
  const limit = parseInt(request.query.limit) || 10;
  const offset = parseInt(request.query.offset) || 0;

  const products = await Products.findAll({ offset, limit });

  reply.send(products);
};

const createProduct = async (request, reply) => {
  const { title, price, qty } = request.body;

  const product = await Products.create({
    id: crypto.randomUUID(), title, price, qty,
  });

  reply.send(product);
};

const updateProduct = async (request, reply) => {
  const id = request.params.uuid;
  const {
    title, price, qty,
  } = request.body;

  await Products.update({
    title, price, qty,
  }, {
    where: {
      id,
    },
  });

  const product = await Products.findOne({
    where: {
      id,
    },
  });

  reply.send(product);
};

const deleteProduct = async (request, reply) => {
  const id = request.params.uuid;

  const products = await Products.findAll({
    where: {
      id,
    },
  });

  if (Array.isArray(products) && products.length === 0) {
    reply.code(400).send({
      message: `product with uuid '${id}' doesnt exists`,
      error: 'DELETE_PRODUCT',
    });
  } else if (Array.isArray(products) && products.length > 0) {
    await Products.destroy({
      where: {
        id,
      },
    });

    reply.send({});
  }
};

const getProduct = async (request, reply) => {
  const id = request.params.uuid;

  const product = await Products.findOne({
    where: {
      id,
    },
  });

  reply.send(product);
};

module.exports = {
  getProductList,
  createProduct,
  updateProduct,
  deleteProduct,
  getProduct,
};
