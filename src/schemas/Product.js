module.exports = {
  type: 'object',
  properties: {
    id: { type: 'string', format: 'uuid' },
    title: { type: 'string' },
    price: { type: 'number' },
    qty: { type: 'number' },
  },
};
