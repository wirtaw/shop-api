FROM node:16
EXPOSE 3000

WORKDIR /home/app

RUN mkdir /home/app/db_data/

COPY package.json /home/app/
COPY package-lock.json /home/app/

RUN npm ci

RUN npx sequelize db:migrate

COPY . /home/app

CMD [ "npm", "start"]