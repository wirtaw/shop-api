const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('product routes GET list', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(6);

  t.test('requests the "/api/v1/products" route success', async tI => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
      query: { limit: 3, offet: 1 },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(responseBody.length, 3);
  });

  t.test('requests the "/api/v1/products" route success empty list', async tI => {
    await Products.destroy({
      where: {
        qty: { $gte: 1 },
      },
      truncate: true,
    });
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(responseBody.length, 0);
  });

  t.test('requests the "/api/v1/products" route not number limit and offset', async tI => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
      query: { limit: 'aa', offset: false },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'querystring.offset should be integer');
  });

  t.test('requests the "/api/v1/products" route not number offset', async tI => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
      query: { limit: 1, offset: 'c' },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'querystring.offset should be integer');
  });

  t.test('requests the "/api/v1/products" route not number limit', async tI => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
      query: { limit: 'aa', offset: 5 },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'querystring.limit should be integer');
  });

  t.test('requests the "/api/v1/products" route no table', async tI => {
    await Products.drop();
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/products',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 500, 'returns a status code of 500');
    tI.equal(responseBody.message, 'SQLITE_ERROR: no such table: Products');
  });
});
