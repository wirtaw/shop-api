const {
  getProductListOpts, createProductOpt, updateProductOpt, deleteProductOpt, getProductOpt,
} = require('../../schemas/routesSchemas');

const models = require('../../../models');

const Products = models.Products;

module.exports = async function (fastify, opts, done) {
  await Products.sync();
  fastify.get('/products', getProductListOpts);
  fastify.post('/product', createProductOpt);
  fastify.put('/product/:uuid', updateProductOpt);
  fastify.delete('/product/:uuid', deleteProductOpt);
  fastify.get('/product/:uuid', getProductOpt);
  done();
};
