const models = require('../../../models');

const configs = require('../../config');

const Products = models.Products;

const protucdStore = new Map();
const cartStore = new Map();

const getProduct = async id => {
  if (protucdStore.has(id)) {
    return protucdStore.get(id);
  }

  const product = await Products.findOne({
    where: {
      id,
    },
  });

  if (product && product.price) {
    const { price, title } = product;
    protucdStore.set(id, { price, id, title });
    return { price, id, title };
  }

  return null;
};

const setProductToCart = product => {
  const { price, title, id } = product;

  if (!cartStore.has(id)) {
    cartStore.set(id, { title, price, qty: 1 });
  } else {
    const item = cartStore.get(id);
    item.qty += 1;

    if (item.qty % 5 !== 0) {
      item.price += price;
    }

    cartStore.set(id, item);
  }
};

const calculateTotal = () => {
  let total = 0;

  cartStore.forEach(value => {
    total += value.price;
  });

  return total;
};

const getCartItems = () => {
  const items = [];

  cartStore.forEach((value, key) => {
    items.push({ id: key, ...value });
  });

  return items;
};

const getCart = async (request, reply) => {
  const { maxLimitTotal, startDiscountTotal, startDiscountValue } = configs.cart;
  const ids = [...request.body].filter(item => !!item);
  let error = '';
  protucdStore.clear();
  cartStore.clear();

  for await (const id of ids) {
    const product = await getProduct(id);

    if (!product) {
      error = `no product with uuid ${id}`;
      break;
    }

    setProductToCart(product);
  }

  if (error) {
    reply.code(400).send({ message: error, error: 'CART_TYPE_ERROR', statusCode: 400 });
  }

  let total = calculateTotal();

  if (total > maxLimitTotal) {
    reply.code(400).send({ message: 'Total amount cannot exceed 100$', error: 'CART_TYPE_ERROR', statusCode: 400 });
  } else {
    if (total >= startDiscountTotal) {
      total -= startDiscountValue;
    }

    reply.send({ items: getCartItems(), total });
  }
};

module.exports = { getCart };
