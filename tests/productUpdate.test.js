const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('product routes PUT', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(4);

  t.test('requests the "/api/v1/product/111" route failed update item (invalid id format)', async tI => {
    const item = {
      title: 'TEST',
      price: 111.5,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'PUT',
      url: '/api/v1/product/111',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'params.uuid should match format "uuid"');
  });

  t.test('requests the "/api/v1/product/:uuid" route success update item', async tI => {
    const item = productMocks[0];
    const { id } = item;
    const uItem = {
      title: 'AAAAA',
      price: 111.5,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'PUT',
      url: `/api/v1/product/${id}`,
      body: {
        ...uItem,
      },
    });

    const responseBody = JSON.parse(response.body);
    const { title, price, qty } = responseBody;

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(title, uItem.title);
    tI.equal(price, uItem.price);
    tI.equal(qty, uItem.qty);
  });

  t.test('requests the "/api/v1/product/:uuid" route failed update item empty title', async tI => {
    const item = productMocks[0];
    const { id } = item;
    const uItem = {
      title: '',
      price: 111.5,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'PUT',
      url: `/api/v1/product/${id}`,
      body: {
        ...uItem,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body.title should NOT be shorter than 3 characters');
  });

  t.test('requests the "/api/v1/product/:uuid" route failed update item price 0', async tI => {
    const item = productMocks[0];
    const { id } = item;
    const uItem = {
      title: 'AAAA',
      price: 0,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'PUT',
      url: `/api/v1/product/${id}`,
      body: {
        ...uItem,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body.price should be >= 0.01');
  });
});
