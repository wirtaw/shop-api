# Shop API

## Prebuild

- Copy `.env.example` to `.env`
- Change `DB_NAME` in the `.env` like `config/config.json` development DB name

## Build

### With npm

- Run `npm ci` to install packages
- Create DB by configs `npx sequelize db:migrate`

### Docker

- To start containers `docker-compose up`

## Develop

- Fill db with seed `npx sequelize db:seed:all`
- Run `npm run dev` development

### Add new model

- Create model with `npx sequelize init:models` with options
- Create `src/routes/v1/MODEL_NAME.js` routes file
- Create `src/hanlers/v1/MODEL_NAME.js` methods to handle routes file
- Register routes to fastify in the `src/server.js`
- Add new category to fastifyOAS in the `tags`
- Create new `src/schemas/routerSchemas.js`

### Update exists routes

- Modify `src/routes/v1/MODEL_NAME.js` routes file to routes
- Modify `src/hanlers/v1/MODEL_NAME.js` methods
- Modify `src/schemas/routerSchemas.js`

### Add new route

- Add to script `src/routes/v1/MODEL_NAME.js` new route
- Create new schemaOpt to this route `src/schemas/routerSchemas.js`
- Crete new handler `src/hanlers/v1/MODEL_NAME.js` to hanler in the schemaOpt

## Documentation (OpenAPI)

 Visit [Docs](http://localhost:3000/docs)

## Project graph (build with [madge](https://www.npmjs.com/package/madge))

![graphShopAPI](./graphShopAPI.svg)

## Test

- Run `npm run test` run tests
