const fastify = require('fastify');
const fsequelize = require('fastify-sequelize');
const fastifyOAS = require('fastify-oas');
const { resolve } = require('path');

const productsRoutes = require('./routes/v1/products');
const cartRoutes = require('./routes/v1/cart');

const configs = require('./config');
const Product = require('./schemas/Product');

function build(opts = {}) {
  const app = fastify(opts);
  const storage = (!['', ':memory:'].includes(configs.sequilize.storage))
    ? resolve(__dirname, configs.sequilize.storage)
    : configs.sequilize.storage;

  app.register(fsequelize, { ...configs.sequilize, storage })
    .ready(err => {
      if (err) {
        console.error('Unable to connect to the database:', err);
      }
    });

  app.register(fastifyOAS, {
    routePrefix: '/docs',
    swagger: {
      info: {
        title: 'Shop API',
        description: 'the fastify swagger api',
        version: '0.1.0',
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here',
      },
      host: 'localhost:3000',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'product', description: 'Products related end-points' },
        { name: 'cart', description: 'Cart related end-points' },
      ],
      definitions: {
        Product,
      },
      securityDefinitions: {
        apiKey: {
          type: 'apiKey',
          name: 'apiKey',
          in: 'header',
        },
      },
    },
    uiConfig: {
      docExpansion: 'full',
      deepLinking: false,
    },
    exposeRoute: true,
  });

  // API routes rigster
  app.register(productsRoutes, { logLevel: 'warn', prefix: '/api/v1/' });
  app.register(cartRoutes, { logLevel: 'warn', prefix: '/api/v1/' });

  return app;
}

module.exports = build;
