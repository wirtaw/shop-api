const crypto = require('crypto');

module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert('Products', [{
      id: crypto.randomUUID(),
      title: 'ProductA',
      price: 1.5,
      qty: 10,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: crypto.randomUUID(),
      title: 'ProductB',
      price: 3.5,
      qty: 100,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: crypto.randomUUID(),
      title: 'ProductC',
      price: 6.5,
      qty: 300,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: crypto.randomUUID(),
      title: 'ProductD',
      price: 4.5,
      qty: 50,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    ], {});
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete('Products', null, {});
  },
};
