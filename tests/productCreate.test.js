const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('product routes POST', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(5);

  t.test('requests the "/api/v1/product" route success create item', async tI => {
    const item = {
      title: 'TEST',
      price: 111.5,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'POST',
      url: '/api/v1/product',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);
    const { title, price, qty } = responseBody;

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(title, item.title);
    tI.equal(price, item.price);
    tI.equal(qty, item.qty);
  });

  t.test('requests the "/api/v1/product" route failed create item', async tI => {
    const item = {
      price: 111.5,
      qty: 999,
    };
    const response = await fastify.inject({
      method: 'POST',
      url: '/api/v1/product',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body should have required property \'title\'');
  });

  t.test('requests the "/api/v1/product" route failed create item empty title', async tI => {
    const item = {
      title: '',
      price: 0,
      qty: null,
    };
    const response = await fastify.inject({
      method: 'POST',
      url: '/api/v1/product',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body.title should NOT be shorter than 3 characters');
  });

  t.test('requests the "/api/v1/product" route failed create item 0 price', async tI => {
    const item = {
      title: 'zfgdg',
      price: 0,
      qty: null,
    };
    const response = await fastify.inject({
      method: 'POST',
      url: '/api/v1/product',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body.price should be >= 0.01');
  });

  t.test('requests the "/api/v1/product" route failed create item -1 qty', async tI => {
    const item = {
      title: 'zfgdg',
      price: 0.23,
      qty: -1,
    };
    const response = await fastify.inject({
      method: 'POST',
      url: '/api/v1/product',
      body: {
        ...item,
      },
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'body.qty should be >= 0');
  });
});
