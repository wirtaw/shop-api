const tap = require('tap');
const build = require('../src/server');

const models = require('../models');

const Products = models.Products;

const productMocks = require('./productMockList');

tap.test('product routes GET', async t => {
  t.before(async () => {
    await Products.sync({ force: true });
  });

  t.beforeEach(async () => {
    await Products.sync({ force: true });
    await Products.bulkCreate(productMocks);
  });

  const fastify = build({});

  await fastify.ready();

  t.plan(2);

  t.test('requests the "/api/v1/product/111" route failed get item (invalid id format)', async tI => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/product/111',
    });

    const responseBody = JSON.parse(response.body);

    tI.equal(response.statusCode, 400, 'returns a status code of 400');
    tI.equal(responseBody.message, 'params.uuid should match format "uuid"');
  });

  t.test('requests the "/api/v1/product/3fa85f64-5717-4562-b3fc-2c963f66afa6" route success get item', async tI => {
    const [item] = productMocks.filter(product => product.id === '3fa85f64-5717-4562-b3fc-2c963f66afa6');
    const response = await fastify.inject({
      method: 'GET',
      url: '/api/v1/product/3fa85f64-5717-4562-b3fc-2c963f66afa6',
    });

    const responseBody = JSON.parse(response.body);
    const { title, price, qty } = responseBody;

    tI.equal(response.statusCode, 200, 'returns a status code of 200');
    tI.equal(title, item.title);
    tI.equal(price, item.price);
    tI.equal(qty, item.qty);
  });
});
