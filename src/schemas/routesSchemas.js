const {
  getProductList, createProduct, updateProduct, deleteProduct, getProduct,
} = require('../handlers/v1/products');
const {
  getCart,
} = require('../handlers/v1/cart');
const Product = require('./Product');

const getProductListOpts = {
  schema: {
    description: 'get product list',
    tags: ['product'],
    summary: 'Get product list',
    querystring: {
      type: 'object',
      required: ['offset', 'limit'],
      properties: {
        offset: {
          type: 'integer',
          description: 'The number of items to skip before starting to collect the result set',
          default: 0,
          minimum: 0,
        },
        limit: {
          type: 'integer',
          description: 'The numbers of items to return',
          default: 10,
          minimum: 1,
        },
      },
    },
    response: {
      200: {
        type: 'array',
        items: Product,
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: getProductList,
};

const createProductOpt = {
  schema: {
    description: 'create new product',
    tags: ['product'],
    summary: 'Create new product',
    body: {
      type: 'object',
      required: ['title', 'price', 'qty'],
      properties: {
        title: { type: 'string', minLength: 3 },
        price: { type: 'number', minimum: 0.01 },
        qty: { type: 'number', minimum: 0 },
      },
    },
    response: {
      200: {
        ...Product,
      },
      400: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: createProduct,
};

const updateProductOpt = {
  schema: {
    description: 'update product',
    tags: ['product'],
    summary: 'update product',
    params: {
      type: 'object',
      required: ['uuid'],
      properties: {
        uuid: {
          type: 'string',
          format: 'uuid',
          description: 'product id in the format UUID',
        },
      },
    },
    body: {
      type: 'object',
      required: ['title', 'price', 'qty'],
      properties: {
        title: { type: 'string', minLength: 3 },
        price: { type: 'number', minimum: 0.01 },
        qty: { type: 'number', minimum: 0 },
      },
    },
    response: {
      200: {
        ...Product,
      },
      400: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: updateProduct,
};

const deleteProductOpt = {
  schema: {
    description: 'delete product',
    tags: ['product'],
    summary: 'delete product',
    params: {
      type: 'object',
      required: ['uuid'],
      properties: {
        uuid: {
          type: 'string',
          format: 'uuid',
          description: 'product id in the format UUID',
        },
      },
    },
    response: {
      200: {
        type: 'object',
      },
      400: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: deleteProduct,
};

const getProductOpt = {
  schema: {
    description: 'get product',
    tags: ['product'],
    summary: 'get product',
    params: {
      type: 'object',
      required: ['uuid'],
      properties: {
        uuid: {
          type: 'string',
          format: 'uuid',
          description: 'product id in the format UUID',
        },
      },
    },
    response: {
      200: {
        ...Product,
      },
      400: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: getProduct,
};

const getCartOpts = {
  schema: {
    description: 'get cart',
    tags: ['cart'],
    summary: 'Get cart',
    body: {
      type: 'array',
      items: {
        type: 'string',
        format: 'uuid',
      },
    },
    response: {
      200: {
        type: 'object',
        properties: {
          items: {
            type: 'array',
            items: Product,
          },
          total: {
            type: 'number',
            minimum: 0,
            maximum: 100,
          },
        },
      },
      400: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
      500: {
        type: 'object',
        properties: {
          statusCode: { type: 'number' },
          error: { type: 'string' },
          message: { type: 'string' },
        },
      },
    },
  },
  handler: getCart,
};

module.exports = {
  getProductListOpts,
  createProductOpt,
  updateProductOpt,
  deleteProductOpt,
  getProductOpt,
  getCartOpts,
};
