const path = require('path');

const envFileName = `.env${process.env.NODE_ENV && `.${process.env.NODE_ENV}`}`;
const pathToEnvFile = path.resolve(__dirname, envFileName);
require('dotenv').config({ path: pathToEnvFile });

const sequilize = {
  instance: 'db',
  autoConnect: false,
  dialect: 'sqlite',
  storage: false,
};

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'production') {
  sequilize.instance = process.env.SEQUELIZE_DB;
  sequilize.autoConnect = !!process.env.SEQUELIZE_CONNECT;
  sequilize.dialect = process.env.SEQUELIZE_DIALECT;
  sequilize.storage = (process.env.DB_PATH && process.env.DB_FILENAME) ? `${process.env.DB_PATH}/${process.env.DB_FILENAME}` : '';
} else {
  sequilize.storage = ':memory:';
}

module.exports = {
  main: {
    port: Number(process.env.PORT) || 3000,
  },
  database: {
    path: process.env.DB_PATH || '../db_data/',
    fileName: process.env.DB_FILENAME || 'db.sqlite',
  },
  sequilize,
  cart: {
    maxLimitTotal: Number(process.env.CART_LIMIT_MAX_TOTAL) || 100,
    startDiscountTotal: Number(process.env.CART_START_DISCOUNT_TOTAL) || 20,
    startDiscountValue: Number(process.env.CART_START_DISCOUNT_VALUE) || 1,
  },
};
